<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::get('/todos', 'TodoController@index');

Route::post('/todos/create','TodoController@store')->name('create');

Route::get('/todos/delete/{todo}','TodoController@delete');

Route::get('/todos/update/{todo}','TodoController@update')->name('update');

Route::post('/todos/updating/{todo}','TodoController@updating');

Route::get('/todos/completed/{todo}','TodoController@completed');


