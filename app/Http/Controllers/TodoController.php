<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Todo;
use Session;


class TodoController extends Controller
{
    public function index(){


    	$alltodos = Todo::latest()->get();

    	return view('todo',compact('alltodos'));
    }


    public function store(Request $request){

       

    	$newtodo = new Todo;
    	$newtodo->todo = request('tod');
    	$newtodo->save();

    	Session::flash('success','you have saved a new work to do');

    	return redirect('/todos');

    }

    public function delete(Todo $todo){    	

    	$todo->delete();

    	Session::flash('success','you have deleted the task');

    	return redirect()->back();
    }



    public function update(Todo $todo){


    	return view('update',compact('todo'));



    }


    public function updating(Todo $todo){



    	$todo->todo = request('tod');

    	$todo->save() ;

    	Session::flash('success','you have updated the task !');

    	return redirect('/todos');
    }

    public function completed(Todo $todo){


    	$todo->completed = 1;

    	Session::flash('success','you have completed the task !');

    	$todo->save();


    	return redirect('/todos');
    }
}
